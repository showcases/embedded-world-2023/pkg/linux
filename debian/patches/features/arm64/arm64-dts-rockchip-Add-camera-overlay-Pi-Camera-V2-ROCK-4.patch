From: Daniel Scally <dan.scally@ideasonboard.com>
Date: Mon, 27 Feb 2023 18:13:24 +0000
Subject: arm64: dts: rockchip: Add camera overlay for Pi Camera V2 on ROCK 4

The ROCK 4 boards support an optional camera. An example of a compatible
camera is the Raspberry Pi Camera Module V2 which contains an IMX219
sensor. Add a corresponding DT overlay.

Co-developed-by: Christopher Obbard <chris.obbard@collabora.com>
[CO: Add makefile & commit message]
Signed-off-by: Christopher Obbard <chris.obbard@collabora.com>
Forwarded: no
---
 arch/arm64/boot/dts/rockchip/Makefile              |  2 +
 .../boot/dts/rockchip/rk3399-rock-pi-4-imx219.dts  | 74 ++++++++++++++++++++++
 2 files changed, 76 insertions(+)
 create mode 100644 arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dts

diff --git a/arch/arm64/boot/dts/rockchip/Makefile b/arch/arm64/boot/dts/rockchip/Makefile
index 0cea090..02b3477 100644
--- a/arch/arm64/boot/dts/rockchip/Makefile
+++ b/arch/arm64/boot/dts/rockchip/Makefile
@@ -75,3 +75,5 @@ dtb-$(CONFIG_ARCH_ROCKCHIP) += rk3568-bpi-r2-pro.dtb
 dtb-$(CONFIG_ARCH_ROCKCHIP) += rk3568-evb1-v10.dtb
 dtb-$(CONFIG_ARCH_ROCKCHIP) += rk3568-odroid-m1.dtb
 dtb-$(CONFIG_ARCH_ROCKCHIP) += rk3568-rock-3a.dtb
+
+dtb-$(CONFIG_ARCH_ROCKCHIP) += rk3399-rock-pi-4b.dtb rk3399-rock-pi-4-imx219.dtbo
diff --git a/arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dts b/arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dts
new file mode 100644
index 0000000..dc464b4
--- /dev/null
+++ b/arch/arm64/boot/dts/rockchip/rk3399-rock-pi-4-imx219.dts
@@ -0,0 +1,74 @@
+// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
+/*
+ * Copyright 2022 Ideas on Board Oy
+ *
+ * Overlay for Raspberry Pi IMX219 camera module connected to the Rock Pi 4
+ */
+
+/dts-v1/;
+/plugin/;
+
+#include <dt-bindings/gpio/gpio.h>
+#include <dt-bindings/pinctrl/rockchip.h>
+
+&{/} {
+	clk_imx219: clock-imx219 {
+		compatible = "fixed-clock";
+		#clock-cells = <0>;
+		clock-frequency = <24000000>;
+		clock-output-names = "clock-imx219";
+	};
+
+	vcc_imx219: vcc-imx219 {
+		compatible = "regulator-fixed";
+		regulator-name = "vcc-imx219";
+
+		gpio = <&gpio1 RK_PB5 GPIO_ACTIVE_HIGH>;
+		enable-active-high;
+		vin-supply = <&vcc_cam>;
+	};
+};
+
+&i2c4 {
+	imx219: camera@10 {
+		compatible = "sony,imx219";
+		reg = <0x10>;
+
+		clocks = <&clk_imx219>;
+
+		VANA-supply = <&vcc_imx219>;
+		VDIG-supply = <&vcc_imx219>;
+		VDDL-supply = <&vcc_imx219>;
+
+		port {
+			imx219_out: endpoint {
+				remote-endpoint = <&isp_in>;
+				data-lanes = <1 2>;
+				clock-noncontinuous;
+				link-frequencies = /bits/ 64 <456000000>;
+			};
+		};
+	};
+};
+
+&mipi_dphy_rx0 {
+	status = "okay";
+};
+
+&isp0 {
+	status = "okay";
+
+	ports {
+		port@0 {
+			isp_in: endpoint {
+				remote-endpoint = <&imx219_out>;
+				data-lanes = <1 2>;
+				clock-noncontinuous;
+			};
+		};
+	};
+};
+
+&isp0_mmu {
+	status = "okay";
+};
